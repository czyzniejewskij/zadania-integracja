## Build & run
Should you need to build and run the solution you can do the following:

* Build: Run build.sh script (if necessary - chmod +x build.sh)  
* Run: Run run.sh script (if necessary - chmod +x run.sh)

or in terminal window run:

* Build: mvn install  
* Run: java -cp target/zadania-integracja-1.0.jar zadania.integracja.Main

3rd task needs ActiveMQ server listening on localhost:5672 (.csv is always produced)