<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:n="http://example.net/library/1.0">

    <xsl:output method="text" />
    <xsl:template match="/">
        <xsl:text>AUTHOR, TITLE, PUBLISHED</xsl:text>
        <xsl:for-each select="n:library/n:books/n:book">
            <xsl:text>&#x0A;</xsl:text>
            <xsl:value-of select="n:author/n:name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="n:author/n:surname" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="n:title" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="n:published" />
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>