package zadania.integracja;

import org.apache.qpid.jms.JmsConnectionFactory;
import javax.jms.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {

        // Task 2 - XSLT

        String xmlFileName="excercise-1.xml";
        String xslFileName="excercise.xsl";
        String outputFileName="excercise-2.csv";

        try {
            xml2csv(xmlFileName, xslFileName, outputFileName);
        }catch (Exception e){
            e.printStackTrace();
        }

        // Task 3 - JMS Topic

        String topic="Example.Library.Publication";
        String host = "localhost";
        String port = "5672";
        String msgContent=null;

        try {
            msgContent = new String(Files.readAllBytes(Paths.get(xmlFileName)), StandardCharsets.UTF_8);
            publish(host, port, topic, msgContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Applies XSL Transformation on an XML file
     * and writes the result to output file.
     * @param xmlFileName    The path to the XML file to be transformed
     * @param xslFileName    The path to the XSL file with transformation definition
     * @param outputFileName The path to the output file
     * @throws TransformerException
     */
    private static void xml2csv(String xmlFileName,
                               String xslFileName,
                               String outputFileName)
            throws TransformerException {

        TransformerFactory factory = TransformerFactory.newInstance();
        Source xsl = new StreamSource(new File(xslFileName));
        Transformer transformer = factory.newTransformer(xsl);
        Source xml = new StreamSource(new File(xmlFileName));
        transformer.transform(xml, new StreamResult(new File(outputFileName)));
    }

    /**
     * Publishes a JMS message.
     * @param host       The address of the server to connect with
     * @param port       The port of the connection
     * @param topic      The topic of the message
     * @param msgContent The content of the message to be send
     * @throws JMSException
     */
    private static void publish(String host,
                               String port,
                               String topic,
                               String msgContent)
            throws JMSException {
        JmsConnectionFactory factory = new JmsConnectionFactory("amqp://" + host + ":" + port);
        Connection connection = factory.createConnection("admin","password");
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destination = session.createTopic(topic);
        MessageProducer producer = session.createProducer(destination);
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        TextMessage message = session.createTextMessage(msgContent);
        producer.send(message);
        connection.close();
    }
}
